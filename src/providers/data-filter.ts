import {Injectable} from '@angular/core';
import {WorkoutService} from './workout-service'


@Injectable()
export class DataFilter {
    items: any;

    constructor(public workoutService: WorkoutService) {
        this.items = workoutService.getWorkouts().subscribe;
    }

    filterItems(searchTerm) {

        return this.items.filter((item) => {
            return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });

    }

}
