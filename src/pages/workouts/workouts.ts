import {Component, NgZone} from '@angular/core';
import {NavController, NavParams, ActionSheetController, Platform, MenuController} from 'ionic-angular';
import {WorkoutDetailsPage} from  '../workout-details/workout-details';
import {AddWorkoutPage} from  '../add-workout/add-workout';
import {LoginPage} from  '../login/login';
import {WorkoutService} from '../../providers/workout-service';
import {AuthData} from '../../providers/auth-data';
import firebase from 'firebase';

@Component({
    selector: 'page-workouts',
    templateUrl: 'workouts.html'
})
export class WorkoutsPage {
    workouts: any;
    uId: any;
    workoutsRef: any;
    workoutList: any;
    loadedWorkoutList: any;

    constructor(public navCtrl: NavController, public workoutService: WorkoutService,
                public actionCtrl: ActionSheetController, public platform: Platform, public menuCtrl: MenuController, public authData: AuthData, private zone: NgZone) {


        this.uId = this.authData.getUser();

        // this.workouts = this.workoutService.getWorkouts();
        this.workoutsRef = firebase.database().ref(`/userProfile/${this.workoutService.userId}/workouts/`);

        this.workoutsRef.on('value', workoutList => {
            let workoutArray = [];
            workoutList.forEach( workout => {
                workoutArray.push(workout.val());
            });

            this.workoutList = workoutArray;
            this.loadedWorkoutList = workoutArray;
            this.zone.run(() => {});
        });





    }

    initializeItems(){
        this.workoutList = this.loadedWorkoutList;
    }

    getItems(searchbar) {
        // Reset items back to all of the items
        this.initializeItems();

        // set q to the value of the searchbar
        var q = searchbar.srcElement.value;


        // if the value is an empty string don't filter the items
        if (!q) {
            return;
        }




        this.workoutList = this.workoutList.filter((v) => {
            if(v.name && q) {
                if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
                    return true;
                } else if (v.weight.toLowerCase().indexOf(q.toLowerCase()) > -1) {
                    return true;
                }
                return false;
            }
        });


    }

    

    createWorkout(): void {
        this.navCtrl.push(AddWorkoutPage);
    }


    returnToLogin() {
        this.navCtrl.setRoot(LoginPage);

    }

    logoutUser() {
        this.authData.logoutUser();
    }

    detailedWorkourView(workoutId) {
        return this.navCtrl.push(WorkoutDetailsPage, {
            workoutId: workoutId
        });
    }


    // workoutDetails(workoutId) {
    //     let action = this.actionCtrl.create({
    //         title: 'Modify your workout',
    //         buttons: [
    //             {
    //                 text: 'Delete',
    //                 role: 'destructive',
    //                 icon: !this.platform.is('ios') ? 'trash' : null,
    //                 handler: () => {
    //                     this.workoutService.removeWorkout(workoutId);
    //                 }
    //             },
    //             {
    //                 text: 'More details',
    //                 icon: !this.platform.is('ios') ? 'play' : null,
    //                 handler: () => {
    //                     this.navCtrl.push(WorkoutDetailsPage, {
    //                         workoutId: workoutId
    //                     });
    //                 }
    //             },
    //             {
    //                 text: 'Cancel',
    //                 role: 'cancel',
    //                 icon: !this.platform.is('ios') ? 'close' : null,
    //                 handler: () => {
    //                     console.log('Cancel clicked');
    //                 }
    //             }
    //         ]
    //     });
    //     action.present();
    // }

}

